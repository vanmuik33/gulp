const gulp = require("gulp");
const sass = require("gulp-sass");
const browserSync = require("browser-sync").create();
const prefix = require("gulp-autoprefixer");
const cssmin = require("gulp-cssnano");
const rename = require("gulp-rename");
const sourcemaps = require("gulp-sourcemaps");
const imagemin = require("gulp-imagemin");
const kit = require("gulp-kit-2");
const cache = require('gulp-cache');
const del = require('del');
const jsMinify = require('gulp-minify');
const useref = require('gulp-useref');
const concat = require('gulp-concat');
const { src } = require("gulp");

function sassCompile() {
    return gulp
        .src("src/assets/scss/**/*.scss")
        .pipe(concat('main.scss'))
        .pipe(sass().on("error", sass.logError))
        .pipe(prefix())
        .pipe(gulp.dest("dist/css/"))
        .pipe(cssmin())
        .pipe(rename({ suffix: ".min" }))
        .pipe(gulp.dest("dist/css/"))
        .pipe(browserSync.stream());
}

function kitCompile() {
    return gulp.src("src/kit/*.kit")
        .pipe(kit())
        .pipe(gulp.dest("dist"))
        .pipe(browserSync.stream());
}

// function jsCombine() {
//     return gulp.src('dist/*.html')
//         .pipe(useref())
//         .pipe(gulp.dest('dist/js/'))
// }

// function jsCombine() {
//     return gulp.src('src/assets/js/**/*.js')
//         .pipe(concat())
//         .pipe(gulp.dest('src'))
// }

function jsOptimize() {
    return gulp.src('src/assets/js/**/*.js')
        .pipe(concat('main.js'))
        .pipe(jsMinify({
            ext: {
                min: '.min.js'
            },
            ignoreFiles: ['-min.js']
        }))
        .pipe(gulp.dest('dist/js'))
        .pipe(browserSync.stream());
}

function imageOptimize() {
    return gulp.src("src/assets/images/**/*.+(png|jpg|gif|svg)")
        .pipe(imagemin())
        .pipe(gulp.dest('dist/image'));
}

function fontsCopy() {
    return gulp.src('src/assets/fonts/**/*')
        .pipe(gulp.dest('dist/fonts'))
}

function cleanDist() {
    return del.sync('dist');
}

function clearCache(callback) {
    return cache.clearAll(callback);
}

async function init() {
    kitCompile();
    sassCompile();
    jsOptimize();
    // jsCombine();
}

function watch() {
    browserSync.init({
        server: {
            baseDir: "dist",
        },
    });
    gulp.watch("src/assets/scss/**/*.scss", sassCompile);
    gulp.watch("src/kit/**/*.kit", kitCompile);
    gulp.watch("src/**/*.html", browserSync.reload);
    gulp.watch("src/assets/js/**/*.js", jsOptimize);
}

async function build() {
    cleanDist();
    kitCompile();
    sassCompile();
    jsOptimize();
    // jsCombine();
    imageOptimize();
    fontsCopy();
}


exports.init = init;
exports.watch = watch;
exports.build = build;